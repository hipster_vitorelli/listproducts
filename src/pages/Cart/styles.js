import styled from "styled-components";
import { darken } from "polished";

export const Container = styled.div`
  padding: 30px;
  background: #fff;
  border-radius: 4px;
  font-family: Roboto, sans-serif;
  display: flex;

  .overflow {
    max-height: 500px;
    overflow-y: scroll;
    padding-right: 5px;

    &::-webkit-scrollbar {
      width: 3px;
    }

    &::-webkit-scrollbar-track {
      background: #fff;
    }

    &::-webkit-scrollbar-thumb {
      background: ${darken(0.06, "#2686ff")};
    }

    &::-webkit-scrollbar-thumb:hover {
      background: #2686ff;
    }
  }

  section {
    width: 100%;
  }

  section.mapa {
    max-width: 380px;
    margin-left: 20px;
    padding-left: 16px;
    border-left: 1px solid #f4f4f4;
  }

  footer {
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    padding-top: 30px;
    width: 100%;

    button {
      background: #2686ff;
      border: 0;
      padding: 12px 22px;
      display: flex;
      align-items: center;
      color: #fff;
      text-transform: uppercase;
      border-radius: 4px;
      transition: background 0.2s;
      font-weight: bold;
      svg {
        margin-right: 5px;
      }
      &:hover {
        background: ${darken(0.03, "#2686ff")};
      }
    }
  }
`;
export const ProductTable = styled.table`
  width: 100%;

  tr {
    border-bottom: 1px solid #ccc;
  }

  thead {
    border-bottom: 1px solid #ccc;
  }

  thead th {
    color: #999;
    text-align: left;
    padding: 12px;
  }

  tbody td {
    padding: 12px;
    border-bottom: 1px solid #eee;
  }

  img {
    max-width: 100px;
    width: 100%;
    max-height: 100px;
    vertical-align: middle;
  }

  strong {
    display: block;
    color: #333;
  }

  span {
    display: inline-block;
    margin-top: 5px;
    font-size: 18px;
    font-weight: bold;
    line-height: 10px;
    padding: 7px;
    background: #e6e6e6;
    border-radius: 40px;
  }

  div {
    display: flex;
    align-items: center;

    input {
      border: 1px solid #ddd;
      border-radius: 4px;
      color: #666;
      padding: 6px;
      width: 50px;
      height: 40px;
      text-align: center;
    }
  }

  button {
    background: none;
    border: 0;
    padding: 6px;
  }
`;
export const Total = styled.div`
  display: flex;
  align-items: baseline;

  span {
    font-size: 14px;
  }

  strong {
    font-weight: bold;
    font-size: 28px;
    margin-left: 5px;
  }
`;

export const MapSection = styled.div``;
export const CartEmpty = styled.div`
  .container {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    max-width: 500px;
    margin: 20px auto;
    text-align: center;
    color: #464646;

    > a {
      display: block;
      margin-top: 20px;
      padding: 12px 22px;
      text-transform: uppercase;
      background: #2686ff;
      text-decoration: none;
      border-radius: 4px;
      color: #fff;
    }
  }
`;
