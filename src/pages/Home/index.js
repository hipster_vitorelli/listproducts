import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { ProductList } from "./styles";
import { MdShoppingCart } from "react-icons/md";
import api from "../../services/api";
import { formatPrice } from "../../util/format";
import * as cartAction from "../../store/modules/cart/actions";

class Home extends Component {
  state = {
    products: []
  };

  async componentDidMount() {
    const response = await api.get(`products`);
    const data = response.data.map(product => ({
      ...product,
      priceFormatted: formatPrice(product.price)
    }));
    this.setState({ products: data });
  }

  handlerAddToCart = id => {
    const { addToCartRequest } = this.props;

    addToCartRequest(id);
  };

  render() {
    const { products } = this.state;
    const { amount } = this.props;

    return (
      <ProductList>
        {products.map(product => (
          <li key={product.id}>
            <img
              src={product.image}
              title={product.title}
              alt={product.title}
            />
            <strong>{product.title}</strong>
            <span>{product.priceFormatted}</span>
            <button
              type="button"
              onClick={() => this.handlerAddToCart(product.id)}
            >
              <div>
                <MdShoppingCart size={16} color="#fff" />{" "}
                {amount[product.id] || 0}
              </div>
              <span>adicionar ao carrinho</span>
            </button>
          </li>
        ))}
      </ProductList>
    );
  }
}

const mapStateToProps = state => ({
  amount: state.cart.reduce((amount, product) => {
    amount[product.id] = product.amount;
    return amount;
  }, {})
});
const mapDispatchToProps = dispatch => bindActionCreators(cartAction, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
