import api from "../../../services/api";
import { call, select, put, all, takeLatest } from "redux-saga/effects";
import { addToCartSuccess, updateAmountSuccess } from "./actions";
import { formatPrice } from "../../../util/format";
// import { toast } from "react-toastify";
import Swal from "sweetalert2";

function* addToCart({ id }) {
  const productExist = yield select(state => state.cart.find(p => p.id === id));

  const stock = yield call(api.get, `/stock/${id}`);
  const stockAmount = stock.data.amount;
  const currentAmount = productExist ? productExist.amount : 0;

  const amount = currentAmount + 1;

  if (amount > stockAmount) {
    Swal.fire({
      title: "Ops! Sem estoque",
      text: "A quantidade selecionada está indisponível",
      icon: "error",
      showConfirmButton: false,
      timer: 2000,
      customClass: "sem-estoque",
      showCloseButton: true
    });
    return;
  }

  if (productExist) {
    const amount = productExist.amount + 1;
    yield put(updateAmountSuccess(id, amount));
  } else {
    const response = yield call(api.get, `/products/${id}`);

    const data = {
      ...response.data,
      amount: 1,
      priceFormatted: formatPrice(response.data.price)
    };

    yield put(addToCartSuccess(data));
  }
}

function* updateAmount({ id, amount }) {
  if (amount <= 0) return;

  const stock = yield call(api.get, `/stock/${id}`);
  const stockAmount = stock.data.amount;

  if (amount > stockAmount) {
    Swal.fire({
      title: "Ops! Sem estoque",
      text: "A quantidade selecionada está indisponível",
      icon: "error",
      showConfirmButton: false,
      timer: 2000,
      customClass: "sem-estoque",
      showCloseButton: true
    });
    return;
  }

  yield put(updateAmountSuccess(id, amount));
}

export default all([
  takeLatest("@cart/ADD_REQUEST", addToCart),
  takeLatest("@cart/UPDATE_AMOUNT_REQUEST", updateAmount)
]);
