import React from "react";
import { Link } from "react-router-dom";
import { MdLocalGroceryStore } from "react-icons/md";
import { Container, Cart } from "./styles";
import logo from "../../assets/images/logo.png";
import { connect } from "react-redux";
function Header({ count }) {
  return (
    <Container>
      <Link to="/">
        <img src={logo} alt="" />
      </Link>

      <Cart to="/cart">
        <div>
          <strong> Meu carrinho </strong>
          <span>{count} item(s)</span>
        </div>
        <MdLocalGroceryStore size={36} color="#fff" />
      </Cart>
    </Container>
  );
}
const mapStateToProps = state => ({
  cart: state.cart,
  count: state.cart.length
});
export default connect(mapStateToProps)(Header);
