import React, { Component } from "react";
import InputMask from "react-input-mask";
import axios from "axios";
import Swal from "sweetalert2";
import { Container } from "./styles";
import MapaContent from "../MapaContent";

import Geocode from "react-geocode";

Geocode.setApiKey("AIzaSyD--qW0DyYtDgwd0g2Gfh6Teq54dMgYM-0");
Geocode.setLanguage("pt");
Geocode.setRegion("pt");

export default class Mapa extends Component {
  state = {
    value: "",
    end: "",
    cep: "",
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
    adress: {},
    show: false
  };

  onMarkerClick = (props, marker, e) => {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true,
      adress: this.props.all,
      show: true
    });
  };

  getCEP(cep) {
    Geocode.fromAddress(cep).then(response => {
      this.setState({
        lat: parseFloat(response.results[0].geometry.location.lat),
        lng: parseFloat(response.results[0].geometry.location.lng)
      });
    });
  }

  onChange = event => {
    this.setState({
      value: event.target.value,
      cep: event.target.value,
      show: false
    });
  };

  async onClick(cep) {
    try {
      const end = await axios.get(`https://viacep.com.br/ws/${cep}/json`);

      if (end.data.erro === true) {
        Swal.fire({
          title: "CEP não encontrado",
          text: "Verifique o valor digitado e tente novamente",
          icon: "error",
          showConfirmButton: false,
          timer: 6000,
          customClass: "sem-estoque",
          showCloseButton: true
        });
        return;
      }

      this.setState({ end: end.data });
      this.getCEP(cep);
    } catch (error) {
      Swal.fire({
        title: "CEP não encontrado",
        text: "Verifique o valor digitado e tente novamente",
        icon: "error",
        showConfirmButton: false,
        timer: 6000,
        customClass: "sem-estoque",
        showCloseButton: true
      });
    }
  }

  beforeMaskedValueChange = (newState, oldState, userInput) => {
    var { value } = newState;
    var selection = newState.selection;
    var cursorPosition = selection ? selection.start : null;

    // keep minus if entered by user
    if (
      value.endsWith("-") &&
      userInput !== "-" &&
      !this.state.value.endsWith("-")
    ) {
      if (cursorPosition === value.length) {
        cursorPosition--;
        selection = { start: cursorPosition, end: cursorPosition };
      }
      value = value.slice(0, -1);
    }

    return {
      value,
      selection
    };
  };

  render() {
    return (
      <Container>
        <div className="container">
          <h3>ENCONTRE SEU ENDEREÇO</h3>
          <br />
          <label>
            <InputMask
              mask="99999-999"
              maskChar={null}
              placeholder="DIGITE SEU CEP"
              value={this.state.value}
              onChange={this.onChange}
              beforeMaskedValueChange={this.beforeMaskedValueChange}
              onBlur={() => this.onClick(this.state.value)}
            />
            <button onClick={() => this.onClick(this.state.value)}>
              Buscar Endereço
            </button>
          </label>
          {this.state.end !== "" ? (
            <div>
              <div className="endereco">
                <h3>{this.state.end.logradouro}</h3>
                <h4>{this.state.end.bairro}</h4>
                <h4>
                  {this.state.end.localidade} - {this.state.end.uf}
                </h4>
                <h4>
                  <strong>CEP {this.state.end.cep}</strong>
                </h4>
              </div>
              <div className="mapaSite">
                <div className="mapa-content">
                  {this.state.show === false ? null : (
                    <MapaContent
                      show={this.state.show}
                      cep={this.state.cep}
                      adress={this.state.end.logradouro}
                      all={this.state.end}
                      lat={this.state.lat}
                      lng={this.state.lng}
                    />
                  )}
                </div>
              </div>
            </div>
          ) : (
            <br />
          )}
        </div>
      </Container>
    );
  }
}
