import React, { Component } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";

export class MapContainer extends Component {
  render() {
    if (!this.props.lat) return null;
    return (
      <Map
        google={this.props.google}
        zoom={16}
        visible={this.props.show}
        initialCenter={{
          lat: this.props.lat,
          lng: this.props.lng
        }}
        style={{
          width: "100%",
          height: "100%",
          position: "relative",
          maxWidth: "362px",
          maxHeight: "362px"
        }}
      >
        {/* <Marker
          title={this.props.cep}
          name={this.props.adress}
          onClick={this.onMarkerClick}
          icon={{
            url: "https://avatars3.githubusercontent.com/u/2029111?s=200&v=4",
            anchor: new window.google.maps.Point(32, 32),
            scaledSize: new window.google.maps.Size(32, 32)
          }}
        />
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}
        >
          <div>
            <h1>{this.state.adress.logradouro}</h1>
            <h3>{this.state.adress.bairro}</h3>
            <h3>
              {this.state.adress.localidade} - {this.state.adress.uf}
            </h3>
            <h3>
              <strong>CEP {this.state.adress.cep}</strong>
            </h3>
          </div>
        </InfoWindow> */}
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyD--qW0DyYtDgwd0g2Gfh6Teq54dMgYM-0"
})(MapContainer);
