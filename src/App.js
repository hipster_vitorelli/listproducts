import React from "react";
import { Router } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import "./config/reactotronConfig";
import Routes from "./routes";
import { Provider } from "react-redux";
import GlobalStyle from "./styles/global";
import Header from "./components/Header/index";
import { store, persistor } from "./store/index";
import { ToastContainer } from "react-toastify";
import history from "./services/history";
function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router history={history}>
          <Header />
          <Routes />
          <GlobalStyle />
          <ToastContainer autoClose={3000} />
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
